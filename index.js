const DATA_ENDPOINTS = {
    'cafes': 'https://raw.githubusercontent.com/debojyoti/places-fake-rest-api/master/cafes.json',

    'places': 'https://raw.githubusercontent.com/debojyoti/places-fake-rest-api/master/places.json'
}

/**
 * summary  : searches details of cafe on input substring
 */
function search() {

    var searchedValue;
    searchedValue = document.getElementById("searchInput").value;
    var cafeName;

    tableBody = document.getElementById("table-body");
    tableRow = tableBody.getElementsByTagName("tr");
    for (var i = 0; i < tableRow.length; i++) {
        cafeNameElement = tableRow[i].getElementsByTagName("td")[0];
        cafeName = cafeNameElement.innerText;
        if (cafeName.indexOf(searchedValue) > -1) {
            tableRow[i].style.display = "";
        } else {
            tableRow[i].style.display = "none";
        }
    }

}

/**
 * summary  : gives view on loading of program
 */

async function firstLook() {
    let cafesPlaces = new Array();
    cafesPlaces = await getData();

    cafesPlaces.map((cPElement, i) => {
        var tr = document.createElement('tr');

        var th = document.createElement('th');
        var tdCafeName = document.createElement('td');
        var tdAddress = document.createElement('td');
        var tdPostalCode = document.createElement('td');
        var tdLat = document.createElement('td');
        var tdLong = document.createElement('td');


        var sNoText = document.createTextNode(i + 1);
        var cafeNameText = document.createTextNode(cPElement.name);
        var AddressText = document.createTextNode(cPElement.locality);
        var postalCodeText = document.createTextNode(cPElement.postal_code);
        var LatText = document.createTextNode(cPElement.lat);
        var LongText = document.createTextNode(cPElement.long);


        th.appendChild(sNoText);
        tdCafeName.appendChild(cafeNameText);
        tdAddress.appendChild(AddressText);
        tdPostalCode.appendChild(postalCodeText);
        tdLat.appendChild(LatText);
        tdLong.appendChild(LongText);

        th.setAttribute("scope", "row");

        tr.appendChild(th);
        tr.appendChild(tdCafeName);
        tr.appendChild(tdAddress);
        tr.appendChild(tdPostalCode);
        tr.appendChild(tdLat);
        tr.appendChild(tdLong);

        document.getElementById("table-body").appendChild(tr);
    })
}


/**
 * summary  : returns array of combined data of cafes and places
 */
async function getData() {
    let cafesPlaces = new Array();
    let cafe = await fetchData('cafes');
    let place = await fetchData('places');
    let placeData = place;

    placeData.map((palceElement) => {
        cafe.map((cafeElement) => {
            if (cafeElement.location_id === palceElement.id) {
                delete palceElement.id;
                var cafeName = cafeElement.name;
                palceElement.name = cafeName;
                cafesPlaces.push(palceElement);
            }
        })
    })

    return cafesPlaces;
}

/**
 * summary: get data from remote 
 *          return array of cafes or places data
 * 
 * args:
 * getCafesOrPlaces(string): either cafes or places      
 */
async function fetchData(getCafesOrPlaces) {
    var data = new Array();
    let response = await fetch(DATA_ENDPOINTS[getCafesOrPlaces]);
    data = await response.json()
    return data[getCafesOrPlaces];
}
